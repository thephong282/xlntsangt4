#Cau a: Tao list tu 0 den 5 buoc nhay 0.1
import decimal
def float_range(start, stop, step):
  while start < stop:
    yield float(start)
    start += decimal.Decimal(step)

print('Cau a: List: ',list(float_range(0, 5.1, '0.1')))
#Cau b: Tinh ham f(x)=2*cos(x)
import math
print('Cau b: f(x) = 2*cos(x)')
x= float(input('       X    = '))
print('       f(x) = ',2*math.cos(x))
