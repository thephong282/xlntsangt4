#Cau 3
A = {1,2,3,4,5,7}
B = {2,4,5,9,12,24}
C = {2,4,8}
print ('A = ', A)
print ('B = ', B)
print ('C = ', C)
#Cau a
for x in C:
    A.add(x)
    B.add(x)
    print ('Cau a: Lap cac phan tu cua C ',x)
#Cau b, In A va B khi dem them phan tu tu C
print ('Cau b: A = ', A)
print ('       B = ', B)
#Cau c, Phan tu giong nhau cua A va B
print ('Cau c: Phan tu giong nhau cua A va B la ', A.intersection(B))
#Cau d, Gop A va B
D= A.union(B)
print('Cau d: Gop A & B = ', D)
#Cau e, In ra ca phan tu A co ma B khong co
print ('Cau e: Phan tu A co ma B khong co la ',(A-B))
#Cau f, Chieu dai cua A va B
print('Cau f : Chieu dai cua A la ', len(A))
print('        Chieu dai cua B la ', len(B))
#Cau g, Gia tri lon nhat cua A gop B
print('Cau g: Gia tri lon nhat la ', max(D))
#Cau h, Gia tri nho nhat cua A gop B
print('Cau h: Gia tri nho nhat la ', min(D))

